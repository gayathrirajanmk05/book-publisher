/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.book.publisher.model.ui.data;

import java.util.Date;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class BookDetails {
    private String bookname;
    private Integer id;
    private String category;
    private Float Prie;
    private Date publishdate;
    private String about;
    private Integer PublisherId;

    public BookDetails(String bookname, Integer id, String category, Float Prie, Date publishdate, String about, Integer PublisherId) {
        this.bookname = bookname;
        this.id = id;
        this.category = category;
        this.Prie = Prie;
        this.publishdate = publishdate;
        this.about = about;
        this.PublisherId = PublisherId;
    }
    private static final Logger LOG = Logger.getLogger(BookDetails.class.getName());

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Float getPrie() {
        return Prie;
    }

    public void setPrie(Float Prie) {
        this.Prie = Prie;
    }

    public Date getPublishdate() {
        return publishdate;
    }

    public void setPublishdate(Date publishdate) {
        this.publishdate = publishdate;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public Integer getPublisherId() {
        return PublisherId;
    }

    public void setPublisherId(Integer PublisherId) {
        this.PublisherId = PublisherId;
    }
    
    
    
}
