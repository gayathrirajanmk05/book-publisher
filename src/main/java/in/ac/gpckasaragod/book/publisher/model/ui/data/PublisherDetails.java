/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.book.publisher.model.ui.data;

/**
 *
 * @author student
 */
public class PublisherDetails {
    private String publishername;
    private Integer id;
    private String publisherdetails;
    private String designation;

    public PublisherDetails(String publishername, Integer id, String publisherdetails, String designation) {
        this.publishername = publishername;
        this.id = id;
        this.publisherdetails = publisherdetails;
        this.designation = designation;
    }

    public String getPublishername() {
        return publishername;
    }

    public void setPublishername(String publishername) {
        this.publishername = publishername;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPublisherdetails() {
        return publisherdetails;
    }

    public void setPublisherdetails(String publisherdetails) {
        this.publisherdetails = publisherdetails;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String deleteBookDetails(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public PublisherDetails ReadPublisherDetails(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    
    
}
