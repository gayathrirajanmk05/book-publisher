/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.book.publisher.service;

import in.ac.gpckasaragod.book.publisher.model.ui.data.PublisherDetails;

import java.util.List;

/**
 *
 * @author student
 */
public interface PublisherDetailService {
    public String savePublisher(String publisherName,String publisherDetails,String designation);
    public PublisherDetails readPublisher();
    public List<PublisherDetails>getAllPublisher();
    public String updatePublisher(String publisherName,String publisherDetails,String designation,Integer id);
    public String deletePublisher(Integer id);
}
