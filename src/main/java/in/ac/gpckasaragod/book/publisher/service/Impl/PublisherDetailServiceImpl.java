/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.book.publisher.service.Impl;

import in.ac.gpckasaragod.book.publisher.service.PublisherDetailService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Flow;
import java.util.concurrent.Flow.Publisher;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class PublisherDetailServiceImpl extends ConnectionServiceImpl implements PublisherDetailService{

    @Override
    public String savePublisher(String publisherName, String publisherDetails, String designation) {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            Connection connection=getConnection();
            Statement statement=connection.createStatement();
            String query = "INSERT INTO PUBLISHERDETAILS(PUBLISHERNAME,PUBLISHERDETAILS,DESIGNATION) VALUES" + "('"+publisherName+"','"+publisherDetails+"','"+designation+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status !=1){
                return "save faild";
            }else{
                return "save successfully";
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(PublisherDetailServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "save failed";
        }
        
    }

    public Publisher readPublisher(Integer id) {
        
    
        try {
            //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            Connection connection=getConnection();
            Statement statement=connection.createStatement();
            String query = "SELECT * FROM PUBLISHERDETAILS WHERE ID="+id;
            System.err.println("Query:"+query);
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                String publisherName = resultSet.getString("PUBLISHERNAME");
                String publisherDetails = resultSet.getString("PUBLISHERDETAILS");
                String designation = resultSet.getString("DESIGNATION");
                Publisher = new Publisher(publisherName,publisherDetails,designation);
                
                
                    
            }
            
            
            
        } catch (SQLException ex) {
            Logger.getLogger(PublisherDetailServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        return publisherDetails;
        
        
    }
      
    @Override
    public List<Publisher>getAllPublisher() {
         List<Publisher>publisherDetails = new ArrayList<Publisher>();
          
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
              String query = "SELECT * FROM PUBLISHERDETAILS";
              System.err.println("Query:"+query);
              ResultSet resultSet = statement.executeQuery(query);
              while(resultSet.next()){
                  String publisherName = resultSet.getString("PUBLISHERNAME");
                  String publisherDetails = resultSet.getString("PUBLISHERDETAILS");
                  String designation = resultSet.getString("DESIGNATION");
                  Publisher publisher = (Publisher) (Flow.Subscriber s) -> {
                      throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
                  };
                  publisher.add(publisher);
              }
        } catch (SQLException ex) {
            Logger.getLogger(PublisherDetailServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return publisherDetails;
    }


    @Override
    public String updatePublisher(String publisherName, String publisherDetails, String designation, Integer id) {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE PUBLISHER SET PUBLISHERNAME";
            System.err.println("Query:"+query);
            int update = statement.executeUpdate(query);
            if(update !=1)
            return "Update successfull";
            else
                return "Update successfully";
        } catch (SQLException ex) {
            Logger.getLogger(PublisherDetailServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    
        return publisherDetails;
              
                 
    }

   

    @Override
    public String deletePublisher(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM PUBLISHER WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Delete successfully";
      } catch (SQLException ex) {
            Logger.getLogger(PublisherDetailServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
   
    }
     
        
    

    
    
    